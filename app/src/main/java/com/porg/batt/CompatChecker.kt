package com.porg.batt

import android.content.Context
import android.os.Build
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.lang.reflect.Field


class CompatChecker {
    companion object {

        /**
         * List of incompatible versions of Samsung's One UI.
         */
        val UNSUPPORTED_ONE_UI_VERSIONS = arrayOf(
            // 6.0 is pretty much only an upgrade
            // Report: https://gitlab.com/narektor/batt/-/issues/5
            "6.0",
            // 6.1 is the launch firmware for the Galaxy S24 series
            // Tested on Galaxy S24 with version S926BXXS2AXEF
            "6.1",
            // 6.1.1 (reported as 6.5) launched with the Galaxy Z Fold6 and Z Flip6
            // Devices upgraded to 6.1.1 don't work (https://gitlab.com/narektor/batt/-/issues/5#note_2112181309),
            // while devices launched with 6.1.1 don't report some of the data correctly
            // (tested on Galaxy Z Flip6 with firmware version F741BXXS1AXHD)
            "6.5"
        )

        fun isIncompatible(context: Context): Boolean {
            // For Samsung and Google use the appropriate check
            if (Build.MANUFACTURER == "samsung") return checkSamsungCompat(context)
            if (Build.MANUFACTURER == "Google") return checkGoogleCompat()
            // For Xiaomi (and Redmi/POCO) check if the device is running MIUI or HyperOS
            if (setOf("xiaomi", "redmi", "poco").contains(Build.BRAND.lowercase())) return isMiui()
            // Otherwise return that the device is compatible
            return false
        }

        private fun checkGoogleCompat(): Boolean {
            // Mark all non-Tensor devices that received Android 14 as incompatible.
            return (
                Build.DEVICE == "redfin"  || // Pixel 5
                Build.DEVICE == "barbet"  || // Pixel 5a
                Build.DEVICE == "bramble" || // Pixel 4a (5G)
                // Also mark the emulator:
                Build.DEVICE.startsWith("emu")
            )
        }

        private fun checkSamsungCompat(context: Context): Boolean {
            return getOneUiVersion(context) in UNSUPPORTED_ONE_UI_VERSIONS
        }

        @Throws(Exception::class)
        fun getOneUiVersion(context: Context): String {
            if (!isSemAvailable(context)) {
                return "" // was "1.0" originally but probably just a dummy value for one UI devices
            }
            val semPlatformIntField: Field =
                Build.VERSION::class.java.getDeclaredField("SEM_PLATFORM_INT")
            val version: Int = semPlatformIntField.getInt(null) - 90000
            return if (version < 0) {
                // not one ui (could be previous Samsung OS)
                ""
            } else (version / 10000).toString() + "." + version % 10000 / 100
        }

        private fun isSemAvailable(context: Context?): Boolean {
            return context != null &&
                    (context.packageManager.hasSystemFeature("com.samsung.feature.samsung_experience_mobile") ||
                            context.packageManager.hasSystemFeature("com.samsung.feature.samsung_experience_mobile_lite"))
        }


        /**
         * Check if the device is running on MIUI.
         *
         * By default, HyperOS is excluded from the check.
         * If you want to include HyperOS in the check, set excludeHyperOS to false.
         *
         * @param excludeHyperOS Whether to exclude HyperOS
         * @return True if the device is running on MIUI, false otherwise
         */
        fun isMiui(excludeHyperOS: Boolean = true): Boolean {
            // This property is present in both MIUI and HyperOS.
            val isMiui = !getProperty("ro.miui.ui.version.name").isNullOrBlank()
            // This property is exclusive to HyperOS only and isn't present in MIUI.
            val isHyperOS = !getProperty("ro.mi.os.version.name").isNullOrBlank()
            return isMiui && (!excludeHyperOS || !isHyperOS)
        }

        // Private function to get the property value from build.prop.
        private fun getProperty(property: String): String? {
            return try {
                Runtime.getRuntime().exec("getprop $property").inputStream.use { input ->
                    BufferedReader(InputStreamReader(input), 1024).readLine()
                }
            } catch (e: IOException) {
                e.printStackTrace()
                null
            }
        }
    }
}