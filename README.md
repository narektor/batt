# Batt <img src="app/src/main/res/mipmap-xhdpi/ic_launcher_round.png" width="30px" alt="Icon"/>

[<img src="https://gitlab.com/IzzyOnDroid/repo/-/raw/master/assets/IzzyOnDroid.png" height="80" alt="Get it on IzzyOnDroid">](https://apt.izzysoft.de/fdroid/index/apk/com.porg.batt) [<img src="https://gitlab.com/narektor/gitlab-badges/-/raw/main/language/en/en-available-on.png" alt="Available on GitLab" height="80">](https://gitlab.com/narektor/batt/-/releases)

A simple app that shows battery status information on Android devices. **Requires Android 14.**

_Featured by [Mishaal Rahman](https://twitter.com/MishaalRahman/status/1664340667525373952), [Android Central](https://www.androidcentral.com/apps-software/android-14-battery-health-insights), [GSMArena](https://m.gsmarena.com/android_14_battery_health_feature-news-58764.php) and [Android Authority](https://www.androidauthority.com/android-14-battery-health-check-3331255/)._

<img height="1000" src="screenshot.png" alt="Screenshot of Batt showing that the battery is healthy, was manufactured on the 8th of August, 2021, and has 734 charge cycles."></img>

## What Batt displays

By default, it shows:
- the number of charge cycles
- the charge status

However, with some setup, it can also show:
- the battery health
- the battery manufacturing date
- the battery first use date

For this to work, either use Shizuku (instructions can be found in the app) or the following ADB command:

```
adb shell pm grant com.porg.batt android.permission.BATTERY_STATS
```

## Technical

The app is built using the traditional View-based system and Material 3 components. To get the `BATTERY_STATS` permission it uses the [Shizuku API](https://github.com/RikkaApps/Shizuku-API).

Make sure to use Android Studio Giraffe or later.

## License
Batt is licensed under the GNU GPL, version 3.

## Acknowledgements
- [Android 14 API Tests](https://gitlab.com/android-api-tests/upside-down-cake) for the base
- [Mishaal Rahman](https://t.me/MishaalAndroidNews/490) for finding and reporting on the new APIs
